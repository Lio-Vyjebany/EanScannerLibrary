//
//  EACodeScanner.swift
//  TestBarcode
//
//  Created by Matus Borodac on 19/02/2021.
//

import SwiftUI

struct QRCodeScan: UIViewControllerRepresentable {
    var scannerViewController: ScannerViewController
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> ScannerViewController {
        scannerViewController.delegate = context.coordinator
        return scannerViewController
    }

    func updateUIViewController(_ vc: ScannerViewController, context: Context) {
    }

    class Coordinator: NSObject, QRCodeScannerDelegate {
        var parent: QRCodeScan

        init(_ parent: QRCodeScan) {
            self.parent = parent
        }
    }
}
