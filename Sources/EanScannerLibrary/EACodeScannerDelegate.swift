//
//  EACodeScannerDelegate.swift
//  TestBarcode
//
//  Created by Matus Borodac on 02/03/2021.
//
protocol QRCodeScannerDelegate {
}

import SwiftUI
import AVFoundation
import UIKit
import Vision


class ScannerViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, ObservableObject {
    
    var codeIsFinded: Bool = false { didSet {
        if codeIsFinded {
            callingView?.showScanner = false
        } else {
            callingView?.showScanner = true
        }  
        callingView?.codeIsFinded = codeIsFinded
    }}
    var code: String = "" { didSet {
        callingView?.code = code
    }}
    @Published var eanCodeReaderIsShowing = false
    private let position = AVCaptureDevice.Position.back
    private var captureDevice: AVCaptureDevice?
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var audioPlayer: AVAudioPlayer?
    var delegate: QRCodeScannerDelegate?
    var callingView: ScannerView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        captureDevice = selectCaptureDevice()
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: captureDevice!) else { return }

        if (captureSession.canAddInput(captureDeviceInput)) {
            captureSession.addInput(captureDeviceInput)
        } else {
            failed()
            return
        }

        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer"))
        
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        } else {
            failed()
            return
        }
        
        guard let connection = videoOutput.connection(with: AVFoundation.AVMediaType.video) else { return }
        guard connection.isVideoOrientationSupported else { return }
        guard connection.isVideoMirroringSupported else { return }
        connection.videoOrientation = .portrait
        connection.isVideoMirrored = position == .back
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }

        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //handle output from video
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return print("CMSSampleBuffer cannot be conveted to ImageBuffer") }
        
        let handler = VNImageRequestHandler(ciImage: CIImage(cvPixelBuffer: imageBuffer), options: [.properties : ""])
        
        guard let _ = try? handler.perform([createBarCodeRequest()]) else {
            return print("Could not perform barcode-request!")
        }
    }
    
    private func createBarCodeRequest() -> VNDetectBarcodesRequest {
        let vnDetectBarcodesRequest = VNDetectBarcodesRequest(completionHandler: {(request, error) in
            // Loopm through the found results
            for result in request.results! {
                if let barcode = result as? VNBarcodeObservation {
                    guard var payload = barcode.payloadStringValue, self.validateEan(payload: payload) else {
                        print("no payload")
                        return
                    }
                    
                    // if ean13 start with 20-29, product is weightable
                    payload = payload.starts(with: "2") && payload.count == 13 ? "\(payload.dropLast(7).dropFirst(2))" : payload
                    
                    self.captureSession.stopRunning()
                    self.playBeep()
                    DispatchQueue.main.async {
                        self.code = payload
                        self.codeIsFinded = true
                    }
                }
                break
            }
        })
        vnDetectBarcodesRequest.symbologies = [.EAN13, .EAN8, .Code39, .Code128]
        return vnDetectBarcodesRequest
    }

    private func selectCaptureDevice() -> AVCaptureDevice? {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: position)
        return deviceDescoverySession.devices[0]
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func validateEan(payload: String) -> Bool {
        var ean = payload
        var even = ""
        var odd = ""
        guard let lastChar = ean.last, let controlNumber = Int(String(describing: lastChar)) else {
            return false
        }
        ean.removeLast()
        ean = String(ean.reversed())
        
        for i in 0..<ean.count {
            if i % 2 == 0 {
                even += "\(ean.charAt(at: i))"
            }
            else {
                odd += "\(ean.charAt(at: i))"
            }
        }
        
        func digitSum(_ n : Int) -> Int {
            return sequence(state: n) { (n: inout Int) -> Int? in
                defer { n /= 10 }
                return n > 0 ? n % 10 : nil
                }.reduce(0, +)
        }
        
        guard let evenDigits = Int(even), let oddDigits = Int(odd) else {
            return false
        }
        // step 1
        let evenDigitSum = digitSum(evenDigits)
        
        // step 2
        let treble = evenDigitSum * 3
        
        // step 3
        let oddDigitsSum = digitSum(oddDigits)
        
        // step 4
        let sum = treble + oddDigitsSum
        
        // step 5
        let rounded: Double = Double(sum) / 10.0
        let roundedValue = Int(rounded.rounded(.up) * 10)
        
        // step 6
        return roundedValue - sum == controlNumber
    }
    
    public func playBeep(){
        
        if let path = Bundle.main.path(forResource: "beep", ofType: "mp3") {
            
            self.audioPlayer = AVAudioPlayer()
            let url = URL(fileURLWithPath: path)
            
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: url)
                self.audioPlayer?.prepareToPlay()
                self.audioPlayer?.play()
            } catch {
                print("Error : Cannot play Audio BEEP")
            }
        } else {
            print("Error : Cannot play Audio BEEP")
        }
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func charAt(at: Int) -> Character {
        let charIndex = self.index(self.startIndex, offsetBy: at)
        return self[charIndex]
    }
}
